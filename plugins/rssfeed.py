import threading
import feedparser
import time
import calendar
import asyncio



class Feeder():
    last_read = None
    bot = None
    running = None
    has_stopped = None
    coro = None
    delayedstart = None

    def __init__(self, bot):
        self.__dict__.update(bot.config.plugins['rssfeed'])
        print("feeder : reading from %s and using delay of %s" % (self.url, self.delay))
        self.bot = bot
        self.running = True
        self.has_stopped = False


    def get_feed(self):
        if self.url != None:
            response = feedparser.parse(self.url)["items"]
            if len(response) > 0:
                return response
            else:
                return None
        else:
            return None


    def post(self, title, link):
        print("posting : " + title)
        self.bot.send_channel_msg("NEWS: " + title + " " + link)


    def start(self):
        q = self.run()
        self.bot.loop.create_task(q)


    async def run(self):
        while self.running:
            await asyncio.sleep(30.0)
            items = self.get_feed()
            if items != None:
                if self.last_read == None:
                    self.last_read = items[0]["link"]
                else:
                    counter = 0

                    while counter < len(items):
                        if items[counter]["link"] == self.last_read:
                            break
                        else:
                            counter += 1

                    if counter == len(items):
                        self.last_read = items[0]["link"]
                    else:
                        while counter > 0:
                            item = items[counter - 1]
                            if calendar.timegm(time.gmtime()) - calendar.timegm(item["published_parsed"]) > self.delay:
                                self.post(item["title"], item["link"])
                                self.last_read = item["link"]
                            counter -= 1

    def stop(self):
        self.running = False

    def process_quit(self):
        pass


def registerplugin(bot):
    plugin = Feeder(bot)
    bot.registercallback("CONNECTED", plugin.start)
    bot.registercallback("SHUTDOWN", plugin.process_quit)

def nameofplugin():
    return "RSS Feed observer"

