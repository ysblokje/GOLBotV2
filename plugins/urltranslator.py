import requests
import asyncio
from html.parser import HTMLParser

class urlt:
    bot = None
    parser = HTMLParser()

    def __init__(self, parent_bot = None):
        self.bot = parent_bot

    async def check_url(self, url):
        try:
            headers = {"Accept-Language": "en-US"}
            future = self.bot.loop.run_in_executor(None, requests.get, url, headers)
            response = await future
            text = response.text
            title = text.split("<title>")[1]
            title = title.split("</title>")[0]
            title = title.strip()
            title = self.parser.unescape(title)
            self.bot.send_channel_msg("URL: " + title)
        except:
            pass

    def process(self, sender_nick, tokens):
        for token in tokens:
            if "http://" in token or "https://" in token:
                q = self.check_url(token)
                self.bot.loop.create_task(q)

def registerplugin(bot):
    plugin = urlt(bot)
    bot.registercallback("PRIVMSG", plugin.process)

def nameofplugin():
    return "Url translator"
