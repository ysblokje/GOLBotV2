#Frotz
import asyncio
import os
from pathlib import Path

class Game(asyncio.SubprocessProtocol):
    def __init__(self, parent):
        self.parent = parent
        parent.game = self

    def connection_made(self, transport):
        self.transport = transport.get_pipe_transport(0)

    def connection_lost(self, reason):
        print(reason)

    def send_command(self, command):
        print("sending %s" % command)
        self.transport.write(str.encode(command + "\n"))

    def pipe_data_received(self, fd, data):
        self.parent.process_response(data)


class Frotz():
    game = None;
    savegame = None
    gamefile = None
    interpreter = None

    def __init__(self, bot):
        self.bot = bot
        config = None
        try:
            config = bot.config.plugins['frotz']
        except:
            pass
        if not config is None:
            self.__dict__.update(config)
        print(bot.storagelocation)
        savegamefolder = Path(bot.storagelocation + "/frotz")

        if not savegamefolder.exists():
            savegamefolder.mkdir()

        if not savegamefolder.is_dir():
            savegamefolder = Path("/tmp")

        self.savegame = savegamefolder / "frotz.qzl"

        savegame = None
        if self.savegame.is_file():
            savegame = str(self.savegame)

        if savegame is None:
            create = self.bot.loop.subprocess_exec(
                lambda: Game(self), self.interpreter, "-m", "-p", self.gamefile, stdin= asyncio.subprocess.PIPE, stdout= asyncio.subprocess.PIPE)
        else:
            create = self.bot.loop.subprocess_exec(
                lambda: Game(self), self.interpreter, "-m", "-p", "-L", savegame, self.gamefile, stdin= asyncio.subprocess.PIPE, stdout= asyncio.subprocess.PIPE)

        bot.loop.run_until_complete(create)
        saveloop = self.bot.loop.call_later(5*60, self.save)

    def process_response(self, text):
        reply = str(text)
        print(reply)
        lines = reply.split("\\n")
        result = []
        for line in lines:
            line.strip()
            if len(line) > 0:
                if line.startswith(">"):
                    continue
                elif line.startswith("b'Please enter a filename"):
                    continue
                elif line.startswith("b'Overwrite existing file"):
                    continue
                elif line == "b'Ok.":
                    continue
                else:
                    result.append(line)

        if len(result)>0:
            q = self.send_response(result)
            self.bot.loop.create_task(q)


    async def send_response(self, lines):
        for line in lines:
            self.bot.send_channel_msg("frotz : %s" % line)
            await asyncio.sleep(1)

    prohibited = [ "quit", "save", "q", "s" ]

    def process_command(self, sender_nick, tokens):
        if len(tokens) <= 1:
            return
        if not tokens[1].startswith("$"):
            return

        tokens[1] = tokens[1][1:]

        if len(tokens[1]) <= 0 or tokens[1] in self.prohibited:
            self.bot.send_channel_msg("HAL : I'm sorry, I can't do that %s." % sender_nick)
            return

        self.game.send_command(" ".join(tokens[1: len(tokens)]))

    def save(self):
        print("saving...")
        if not self.game is None:
            self.game.send_command("save")
            self.game.send_command(str(self.savegame))
            if os.path.isfile(self.savegame):
                self.game.send_command("yes")
            saveloop = self.bot.loop.call_later(5*60, self.save)


def registerplugin(bot):
    plugin = Frotz(bot)
    bot.registercallback("PRIVMSG", plugin.process_command)
    bot.registercallback("SHUTDOWN", plugin.save)

def nameofplugin():
    return "Frotz interpreter"

