# regex plugin
#import Plugin
import datetime

class regex:
    bot = None
    last_message = {}
    def __init__(self, parent_bot = None):
        self.bot = parent_bot

    def process(self, sender_nick, tokens):
        valid = False

        if tokens[0].startswith("s/") and tokens[len(tokens)-1].endswith("/"):
            unsplit_line = " ".join(tokens[:])
            replace_strings = unsplit_line.split("/")
            print("regex : %s" % replace_strings)
            if len(replace_strings) >= 3:
                to_replace = replace_strings[1]
                replace_with = replace_strings[2]
                try:
                    result = self.last_message[sender_nick][0].replace(to_replace, replace_with)
                    self.bot.send_channel_msg("%s meant to say: '%s'" %
                        (sender_nick, result))
                except:
                    True

                valid = True

        if not valid:
            self.last_message[sender_nick] = (" ".join(tokens[:]), datetime.datetime.utcnow())

    def process_part(self, sender_nick, tokens):
        try:
            del self.last_message[sender_nick]
        except:
            True

def registerplugin(bot):
    plugin = regex(bot)
    bot.registercallback("PRIVMSG", plugin.process)
    bot.registercallback("PART", plugin.process_part)

def nameofplugin():
    return "Regex"
