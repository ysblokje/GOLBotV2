import sys
import traceback

def print_exception(message="", exc_info=sys.exc_info(), exception = traceback.format_exc()):
    print(message)
    print("Backtrace :")
    print(traceback.format_exc())
    print(exc_info[0])
