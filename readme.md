# Golbot
An IRC bot intended for use on the #gamingonlinux IRC channel on Freenode. It can easily work on other channels as well though.

## Config
By default the golbot looks in /etc/goldbot/config.yml 
The contents are fairly self explainatory :

	nick: xxxBolbot
	server: irc.freenode.org
	#channelname is not preceded by # 
	channel: golbottest
	ident: xxxGolbot
	realname: I have none

For plugin configuration you just add your stanza to the plugins tag, like so :

	plugins:
	        rssfeed:
	                #url to fetch from
	                url: http://lorem-rss.herokuapp.com/feed
	                #how old must an addition be before start showing it
	                delay: 180

## Plugins
Golbot comes with a couple of plugins


### Regex
A basic s/find/replace/ plugin.
Whenever somebody send the message like 's/find/replace/' it wil try to correct the previous message send by that person and post the message to the channel.


### Url translator
When somebody posts one or more url's pointing to http:// or https:// addresses it will try to resolve those links and post the message to the channel 

### RSS Feed observer
Shall try to scan a rss feed and post headlines to the channel

### Presence
Can be asked about the presence of people on the channel. 
Commands :
- seen
- tell

### Writing your own plugin
To write your own plugin is easy, just create a file in the plugin folder containing at least the following functions 

- nameofplugin this function must return  a human readable name for the plugin
- registerplugin(bot) this function is called so you have a chance to setup your plugin

The way the plugins get used is via an string based event system. 
Currently those events are : 

- SHUTDOWN
- CONNECTED
- JOIN
- PART
- PRIVMSG
- QUIT
- 353

With the exception of SHUTDOWN and CONNECTED the events follow the IRC naming scheme.
To make your plugin do anything you just need to register your callbacks to those events. Here is some example code to illustrate.
	
	def hello():
		print("Hello World!")
	
	def registerplugin(bot):
	    bot.registercallback("CONNECTED", hello)
	
	def nameofplugin():
	    return "HelloWorld"

#### Configuration
It is possible to store configuration for your plugin in the config.yml file by expanding on the plugins: stanza. To access the values stored there you can use GOLBot.config.plugins['nameused'] for an example of this look at the rssfeed plugin. If you wish to have your own configuration file, there's nothing stopping you and the location for storing config files is obtained by using the GOLBot.configlocation variable.

#### Storage 
If you need to store data the location to do so is stored in GOLBot.storagelocation
